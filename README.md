# OSWPR - Open Source Without Politics and Racism (fascism and any other form of discrimination). Just IT.

# Описание работы

Сканер ищет в заданном каталоге рекурсивно все файлы, содержащие

 * ключевые слова из словарей
 * любые IPv4 - адреса
 * парсит из атакующих коммитов никнеймы авторов аккаунтов github

# Варинаты применения

Можно запускать руками или добавить в CI/CD например в папке vendors/ или вообще во всем проекте при обновлении.

# Словари
~~~
words - ключевые слова
authors - авторы репозиториев (никнеймы)
~~~

Пересобрать словари (после добавления слов). Скрипт сортирует все слова, убирает дубли и приводит весь все к нижнему регистру.
~~~
./dics_processing.sh
~~~

# Контент для сканирования
~~~
content/
~~~

# Запуск
~~~
chmod +x scan.sh
./scan.sh
~~~

# Как парсить никнеймы зловредных коммитеров / преступников / авторов

Вход: парсер читает файл malicious-commits.txt  

Выход: пишет в файл malicious-authors.txt никнеймы списком

Далее: можно добавить полученные список в словарь authors для сканера

~~~
./malicious-authors.sh
~~~

# Полезное

Список малвари, шифровальщиков и прочих военных действий в OpenSource продуктах

[https://docs.google.com/spreadsheets/d/1H3xPB4PgWeFcHjZ7NOPtrcya_Ua4jUolWm-7z9-jSpQ/htmlview?pru=AAABf8DOHu8*oeLhjzbwEEuMkI8Jdmyi1w#](https://docs.google.com/spreadsheets/d/1H3xPB4PgWeFcHjZ7NOPtrcya_Ua4jUolWm-7z9-jSpQ/htmlview?pru=AAABf8DOHu8*oeLhjzbwEEuMkI8Jdmyi1w#)
