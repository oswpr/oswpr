#!/bin/bash
cat malicious-commits.txt | xargs -I % curl -L % | grep 'commit-author' | sed -n 's/<a.*>\(.*\)<\/a>/\1/Ip' | xargs | awk -F' ' '{ for(i=1;i<=NF;i++) print $i }' > malicious-authors.txt
