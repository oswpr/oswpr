#!/bin/bash
echo
echo "Found Words in files:"
echo
find ./content -type f | grep -v 'words' | grep -v 'authors' | xargs -I % grep -i --with-filename -f words %
echo
echo "Found Authors in files:"
echo
find ./content -type f | grep -v 'authors' | grep -v 'words' | xargs -I % grep -i --with-filename -f authors %
echo
echo
echo "Found IPv4 hardcoded in files:"
echo
find ./content -type f | grep -v 'authors' | grep -v 'words' | xargs -I % grep -i --with-filename -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" %
echo
