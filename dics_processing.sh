#!/bin/bash
awk '{print tolower($0)}' words > tmp && mv tmp words
cat words | sort | uniq > tmp && mv tmp words
awk '{print tolower($0)}' authors > tmp && mv tmp authors
cat authors | sort | uniq > tmp && mv tmp authors
